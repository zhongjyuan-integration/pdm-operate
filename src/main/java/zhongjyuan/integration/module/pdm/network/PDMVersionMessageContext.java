package zhongjyuan.integration.module.pdm.network;

import zhongjyuan.integration.module.network.VersionMessageContext;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.command.IPacketCommandToken;
import zhongjyuan.integration.module.network.message.DuplicateMessageTokenException;
import zhongjyuan.integration.module.network.message.IMessage;
import zhongjyuan.integration.module.network.message.MessageTokenException;

/**
 * @className: PDMVersionMessageContext
 * @description: PDM版本消息上下文类。继承自{@link VersionMessageContext} ，用于处理PDM的版本消息。
 * @author: zhongjyuan
 * @date: 2023年11月1日 下午4:27:18
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class PDMVersionMessageContext extends VersionMessageContext {

	/**
	 * 默认构造函数
	 */
	public PDMVersionMessageContext() {

	}

	/**
	 * put方法用于将特定IMessage类与一个IPacketCommandToken关联起来，并将其放入上下文中。
	 * <p>首先通过resolvedToken方法获取与IMessage类关联的IPacketCommandToken（如果存在）。</p>
	 * <p>如果获取到的token不为空，则判断该token是否已经存在于上下文中，如果已存在则抛出DuplicateMessageTokenException异常。</p>
	 * <p>如果不存在重复的token，则将IMessage类和token放入上下文中。</p>
	 *
	 * @param clazz 要放入上下文中的IMessage类
	 * @return IPacketCommandToken与IMessage类相关联的token
	 * @throws MessageTokenException 当存在重复的token时抛出异常
	 */
	public IPacketCommandToken put(Class<? extends IMessage> clazz) throws MessageTokenException {

		IPacketCommandToken token = resolvedToken(clazz);

		if (null != token) {

			Class<? extends IMessage> originClazz = get(token);

			if (null != originClazz)
				throw new DuplicateMessageTokenException(token, clazz, originClazz);

			put(clazz, token);
		}

		return token;
	}

	/**
	 * isPermitted方法用于判断给定的Token是否允许。
	 * 当Token不为空时返回true，否则返回false。
	 *
	 * @param token 要判断的Token
	 * @return 是否允许
	 */
	protected boolean isPermitted(Token token) {
		return (null != token);
	}
}
