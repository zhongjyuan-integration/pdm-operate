package zhongjyuan.integration.module.pdm.network.service;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Injector;

import zhongjyuan.integration.module.network.MessageContext;
import zhongjyuan.integration.module.network.configurate.IServiceConfigurate;
import zhongjyuan.integration.module.network.message.IInputMessage;
import zhongjyuan.integration.module.network.message.IMessage;
import zhongjyuan.integration.module.network.message.MessageTokenException;
import zhongjyuan.integration.module.network.service.IService;
import zhongjyuan.integration.module.network.service.IServiceMaker;
import zhongjyuan.integration.module.network.service.proxy.ClusterProxyClient;
import zhongjyuan.integration.module.pdm.BootAdapter;
import zhongjyuan.integration.module.pdm.network.PDMVersionMessageContext;
import zhongjyuan.integration.module.pdm.network.configurate.PDMServiceConfigurate;
import zhongjyuan.integration.module.pdm.network.configurate.PDMServiceConfigurate.AddressConfigurate;
import zhongjyuan.integration.module.utils.ClassUtil;

/**
 * @className: PDMProxyClient
 * @description: PDM代理客户端类，继承自{@link ClusterProxyClient} 类
 * @author: zhongjyuan
 * @date: 2023年11月1日 下午5:09:15
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class PDMProxyClient extends ClusterProxyClient {

	// 日志记录器
	private final static Logger LOGGER = LoggerFactory.getLogger(PDMProxyClient.class.getName());

	// 依赖注入器
	private final Injector injector;

	// 消息上下文对象
	private MessageContext context;

	@Inject
	private PDMProxyClient(Injector injector) {
		if (null == injector)
			throw new IllegalArgumentException("Null injector!");

		this.injector = injector;

		try {
			// 配置并启动PDM客户端
			configure();
		} catch (Exception e) {
			LOGGER.error("Exception caught at constructs the PDM client: {}", e.getMessage(), e);
		}
	}

	/**
	 * 配置和启动服务
	 *
	 * @throws Exception 当配置和启动服务过程中发生异常时抛出
	 */
	final void configure() throws Exception {
		final AddressConfigurate addressConfig = (AddressConfigurate) this.injector.getInstance(AddressConfigurate.class);
		// 获取AddressConfig对象实例

		IServiceConfigurate serviceConfigurate = new PDMServiceConfigurate(addressConfig);
		// 根据AddressConfig创建IServiceConfigurate实例

		Class<? extends IService> serviceType = serviceConfigurate.getServiceType();
		// 获取服务类型

		IService service = (IService) this.injector.getInstance(serviceType);
		// 获取服务实例

		IServiceMaker serviceMaker = service.getServiceMaker(serviceConfigurate);
		// 根据IServiceConfigurate创建IServiceMaker实例

		this.injector.injectMembers(serviceMaker);
		// 注入成员依赖项到IServiceMaker实例

		serviceMaker.withContext(getMessageContext());
		// 使用消息上下文设置IServiceMaker实例的上下文

		serviceMaker.build();
		// 构建IServiceMaker实例

		service.run();
		// 运行服务

		proxy(service);
		// 代理服务
	}

	/**
	 * 获取消息上下文对象
	 *
	 * @return 消息上下文对象
	 * @throws MessageTokenException 当处理消息token时发生异常时抛出
	 */
	@SuppressWarnings("unchecked")
	public MessageContext getMessageContext() throws MessageTokenException {
		// 如果context为空，则进行初始化
		if (null == this.context) {

			// 创建一个PDMVersionMessageContext对象，并将其转换为MessageContext类型
			this.context = (MessageContext) new PDMVersionMessageContext();

			// 使用ClassUtil工具类获取指定包下的所有类
			Set<Class<?>> classes = ClassUtil.getClasses(BootAdapter.class.getPackage(), getClass().getClassLoader());
			if (!classes.isEmpty())
				for (Class<?> cla : classes) {
					// 判断该类是否实现了IInputMessage接口
					if (IInputMessage.class.isAssignableFrom(cla))
						// 将该类添加到上下文中
						this.context.put((Class<? extends IMessage>) cla);
				}
		}

		// 返回消息上下文对象
		return this.context;
	}
}
