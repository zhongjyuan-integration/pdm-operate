package zhongjyuan.integration.module.pdm.network.configurate;

import java.lang.annotation.Annotation;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.mina.core.service.IoHandler;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.demux.DemuxingProtocolCodecFactory;
import org.apache.mina.filter.codec.demux.MessageDecoder;
import org.apache.mina.filter.codec.demux.MessageEncoder;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import zhongjyuan.integration.module.network.annotation.GeneralToken;
import zhongjyuan.integration.module.network.configurate.IInetAddressEntry;
import zhongjyuan.integration.module.network.configurate.IProtocolCodecConfigurate;
import zhongjyuan.integration.module.network.configurate.IServiceConfigurate;
import zhongjyuan.integration.module.network.handler.SimpleHandlerAdapter;
import zhongjyuan.integration.module.network.message.IOutputMessage;
import zhongjyuan.integration.module.network.protocol.codec.VersionMessageDecoder;
import zhongjyuan.integration.module.network.protocol.codec.VersionMessageEncoder;
import zhongjyuan.integration.module.network.service.AbstractProxyService;
import zhongjyuan.integration.module.network.service.ClusterClient;
import zhongjyuan.integration.module.network.service.IService;

/**
 * @className: HttpServiceConfigurate
 * @description: PDM服务配置类
 * <p>该类实现了{@link IServiceConfigurate} 接口，用于设置PDM服务的基本信息、参数和相关类的类型。</p>
 * <p>此外，该类还实现了{@link IProtocolCodecConfigurate} 接口，用于配置协议编解码器的参数和设置。</p>
 * @author: zhongjyuan
 * @date: 2023年11月3日 上午11:02:24
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class PDMServiceConfigurate implements IServiceConfigurate {

	private static final long serialVersionUID = 1L;

	AddressConfigurate addressConfigurate;

	/**
	 * 构造函数
	 * @param addressConfigurate 远程地址配置对象
	 */
	public PDMServiceConfigurate(AddressConfigurate addressConfigurate) {
		this.addressConfigurate = addressConfigurate;
	}

	/**
	 * 返回服务配置的ID。
	 * @return 服务配置的ID。
	 */
	@Override
	public String getId() {
		return "pdm-client";
	}

	/**
	 * 返回服务的名称。
	 * @return 服务的名称。
	 */
	@Override
	public String getName() {
		return "PDM客户端";
	}

	/**
	 * 返回服务是否启用日志记录。
	 * @return 如果启用了日志记录则返回true，否则返回false。
	 */
	@Override
	public boolean isLogging() {
		return true;
	}

	/**
	 * 返回服务是否启用数据压缩。
	 * @return 如果启用了数据压缩则返回true，否则返回false。
	 */
	@Override
	public boolean isCompressing() {
		return false;
	}

	/**
	 * 返回服务的类型。
	 * @return 服务的类型。
	 */
	@Override
	public Class<? extends IService> getServiceType() {
		return ClusterClient.class;
	}

	/**
	 * 返回代理服务的类型。
	 * @return 代理服务的类型。
	 */
	@Override
	public Class<? extends AbstractProxyService> getProxyServiceType() {
		return null;
	}

	/**
	 * 返回服务的协议编解码配置。
	 * @return 服务的协议编解码配置。
	 */
	@Override
	public IProtocolCodecConfigurate getProtocolCodecConfigurate() {
		// 创建并返回一个匿名内部类，实现了IProtocolCodecConfigurate接口
		return new IProtocolCodecConfigurate() {

			private static final long serialVersionUID = 1L;

			/**
			 * 返回服务使用的字节顺序。
			 * @return 服务使用的字节顺序。
			 */
			@Override
			public ByteOrder getByteOrder() {
				return null;
			}

			/**
			 * 返回用于协议编解码的工厂的名称。
			 * @return 用于协议编解码的工厂的名称。
			 */
			@Override
			public String getFactoryName() {
				return getFactoryType().getName();
			}

			/**
			 * 返回用于协议编解码的工厂的类型。
			 * @return 用于协议编解码的工厂的类型。
			 */
			@Override
			public Class<? extends ProtocolCodecFactory> getFactoryType() {
				return DemuxingProtocolCodecFactory.class;
			}

			/**
			 * 返回用于协议编解码的处理器的名称。
			 * @return 用于协议编解码的处理器的名称。
			 */
			@Override
			public String getHandlerName() {
				return getHandlerType().getName();
			}

			/**
			 * 返回用于协议编解码的处理器的类型。
			 * @return 用于协议编解码的处理器的类型。
			 */
			@Override
			public Class<? extends IoHandler> getHandlerType() {
				return SimpleHandlerAdapter.class;
			}

			/**
			 * 返回服务的消息编码器配置列表。
			 * @return 服务的消息编码器配置列表。
			 */
			@Override
			public List<IMessageEncoderConfigurate> getEncoderConfigurates() {
				ArrayList<IMessageEncoderConfigurate> list = Lists.newArrayList();
				list.add(new IMessageEncoderConfigurate() {

					/**
					 * 返回编码器的名称。
					 * @return 编码器的名称。
					 */
					@Override
					public String getName() {
						return getEncoderType().getName();
					}

					/**
					 * 返回消息的名称。
					 * @return 消息的名称。
					 */
					@Override
					public String getMessageName() {
						return getMessageType().getName();
					}

					/**
					 * 返回消息的类型。
					 * @return 消息的类型。
					 */
					@Override
					public Class<?> getMessageType() {
						return IOutputMessage.class;
					}

					/**
					 * 返回编码器的类型。
					 * @return 编码器的类型。
					 */
					@Override
					public Class<? extends MessageEncoder<?>> getEncoderType() {
						return VersionMessageEncoder.class;
					}
				});

				return list;
			}

			/**
			 * 返回服务的消息解码器配置列表。
			 * @return 服务的消息解码器配置列表。
			 */
			@Override
			public List<IMessageDecoderConfigurate> getDecoderConfigurates() {
				List<IMessageDecoderConfigurate> list = Lists.newArrayList();
				list.add(new IMessageDecoderConfigurate() {

					/**
					 * 返回解码器的名称。
					 * @return 解码器的名称。
					 */
					@Override
					public String getName() {
						return getDecoderType().getName();
					}

					/**
					 * 返回解码器的类型。
					 * @return 解码器的类型。
					 */
					@Override
					public Class<? extends MessageDecoder> getDecoderType() {
						return VersionMessageDecoder.class;
					}
				});

				return list;
			}

		};
	}

	/**
	 * 返回服务的IP地址条目映射。
	 * @return 服务的IP地址条目映射。
	 */
	@Override
	public Map<String, IInetAddressEntry> getInetAddressEntries() {
		Map<String, IInetAddressEntry> address = Maps.newHashMap();
		address.put("default", new IInetAddressEntry() {

			private static final long serialVersionUID = 1L;

			/**
			 * 返回IP地址条目的ID。
			 * @return IP地址条目的ID。
			 */
			@Override
			public String getId() {
				return "default";
			}

			/**
			 * 返回IP地址条目的端口号。
			 * @return IP地址条目的端口号。
			 */
			@Override
			public int getPort() {
				return getSocketAddress().getPort();
			}

			/**
			 * 返回IP地址条目的主机名。
			 * @return IP地址条目的主机名。
			 */
			@Override
			public String getHost() {
				return getSocketAddress().getHostName();
			}

			/**
			 * 返回IP地址条目的套接字地址。
			 * @return IP地址条目的套接字地址。
			 */
			@Override
			public InetSocketAddress getSocketAddress() {
				return (InetSocketAddress) addressConfigurate.getRemoteAddress();
			}

			@Override
			public String toString() {
				StringBuilder sb = new StringBuilder();
				sb.append("[ ").append("id=").append(getId()).append(", host=").append(getHost()).append(", port=").append(getPort()).append(" ]");
				return sb.toString();
			}
		});

		return address;
	}

	/**
	 * 返回服务令牌的域注解类。
	 * @return 服务令牌的域注解类。
	 */
	@Override
	public Class<? extends Annotation> getServiceTokenDomain() {
		return null;
	}

	/**
	 * 返回服务令牌的上下文注解类。
	 * @return 服务令牌的上下文注解类。
	 */
	@Override
	public Class<? extends Annotation> getServiceTokenContext() {
		return GeneralToken.class;
	}

	/**
	 * @className: AddressConfigurate
	 * @description: 远程地址配置对象
	 * @author: zhongjyuan
	 * @date: 2023年11月1日 下午4:25:07
	 * @version: 2023.02.01
	 * @copyright: Copyright (c) 2023 zhongjyuan.com
	 */
	public static class AddressConfigurate {

		/**
		 * pdmAddress字段表示PDM服务的地址信息。
		 */
		@Inject
		@Named("pdm.address")
		private String pdmAddress;

		/**
		 * getRemoteAddress方法返回远程地址。
		 * 如果pdmAddress不为空，则解析pdmAddress字符串获取主机名和端口号。
		 * 如果解析成功，则返回对应的InetSocketAddress对象。
		 * 如果解析失败或pdmAddress为空，则返回默认的InetSocketAddress对象（使用本地主机和5555端口）。
		 *
		 * @return 远程地址的InetSocketAddress对象。
		 */
		public SocketAddress getRemoteAddress() {
			// 首先判断pdmAddress是否为空
			if (null != this.pdmAddress && !this.pdmAddress.isEmpty()) {

				// 如果pdmAddress不为空，那么尝试用“:”作为分隔符将其拆分成主机名和端口号两部分
				String[] arr = this.pdmAddress.split(":", 2);

				// 判断拆分后的数组是否为空并且长度大于0
				if (null != arr && arr.length > 0)
					// 如果拆分成功，就返回一个新的InetSocketAddress对象，其中包含主机名和端口号
					return new InetSocketAddress(arr[0], Integer.parseInt(arr[1]));
			}

			// 如果pdmAddress为空或者解析失败，则返回一个默认的InetSocketAddress对象，其中包含本地主机和5555端口
			return new InetSocketAddress("127.0.0.1", 5555);
		}
	}

}