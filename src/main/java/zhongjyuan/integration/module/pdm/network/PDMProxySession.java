package zhongjyuan.integration.module.pdm.network;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.future.IoFutureListener;
import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;

import com.google.inject.Inject;

import zhongjyuan.integration.module.network.message.AbstractDuplexSyncMessage;
import zhongjyuan.integration.module.network.message.SyncMessageRecord;
import zhongjyuan.integration.module.network.service.IClient;
import zhongjyuan.integration.module.pdm.network.service.PDMProxyClient;

/**
 * @className: PDMProxySession
 * @description: PDMProxySession 负责管理与 PDM 服务器的网络连接。
 * @author: zhongjyuan
 * @date: 2023年11月1日 下午5:31:51
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class PDMProxySession {

	@Inject
	private Logger LOGGER;

	/**
	 * 与PDM代理服务器之间的I/O会话
	 */
	private IoSession session;

	/**
	 * PDM代理客户端
	 */
	private final IClient client;

	/**
	 * 用于同步访问会话和客户端的锁
	 */
	private final ReentrantLock lock = new ReentrantLock();

	/**
	 * 构造函数，使用给定的 PDMProxyClient 创建 PDMProxySession。
	 * 
	 * @param client 用于创建网络连接的 PDMProxyClient。
	 */
	@Inject
	public PDMProxySession(PDMProxyClient client) {
		if (null == client)
			throw new IllegalArgumentException("I/O service context was null!");

		this.client = (IClient) client;
	}

	/**
	 * 检查是否有活动的网络会话。
	 * 
	 * @return 如果有活动的会话，则返回 true；否则返回 false。
	 */
	boolean hasLiveSession() {
		return (null != this.client && null != this.session && this.session.isConnected());
	}

	/**
	 * 获取 IoSession 实例。
	 * 
	 * @return IoSession 实例。
	 */
	public IoSession getSession() {

		if (!hasLiveSession()) {

			try {

				this.lock.lock();

				if (!hasLiveSession()) {

					final CountDownLatch countDownLatch = new CountDownLatch(1);

					// 使用异步连接方法创建会话
					this.client.connect(new IoFutureListener<ConnectFuture>() {

						public void operationComplete(ConnectFuture future) {

							if (future.isConnected() || future.isDone())
								PDMProxySession.this.session = future.getSession();

							countDownLatch.countDown();
						}
					});

					try {
						countDownLatch.await(10000L, TimeUnit.MILLISECONDS);
					} catch (Exception e) {
						LOGGER.error("Exception caught at retrieving the PDMProxyClient session: {}", e.getMessage(), e);
					}
				}
			} finally {
				this.lock.unlock();
			}
		}

		return this.session;
	}

	/**
	 * 将 AbstractDuplexSyncMessage 写入网络会话并等待响应。
	 * 
	 * @param message 要写入的 AbstractDuplexSyncMessage。
	 * @return 表示写入消息的 SyncMessageRecord。
	 * @throws Exception 写入消息时发生错误时抛出异常。
	 */
	public SyncMessageRecord write(AbstractDuplexSyncMessage message) throws Exception {
		if (null == message)
			return null;

		IoSession session = getSession();
		if (null == session)
			throw new IllegalArgumentException("Illegal session for PDM!");

		// 创建表示消息记录的 SyncMessageRecord 对象
		SyncMessageRecord messageRecord = new SyncMessageRecord(message, new Object());

		// 将消息记录存储在会话中，以便在接收到响应时进行处理
		session.setAttribute(Integer.valueOf(message.commandId()), messageRecord);

		// 将消息写入会话
		session.write(message);

		// 等待消息记录被处理
		messageRecord.lock();

		return messageRecord;
	}

	/**
	 * 关闭网络会话。
	 */
	public void close() {
		if (null != this.session)
			this.session.closeNow();
	}
}
