package zhongjyuan.integration.module.pdm;

import org.apache.mina.core.service.IoConnector;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import com.google.inject.Scopes;

import zhongjyuan.integration.module.AbstractModuleService;
import zhongjyuan.integration.module.pdm.network.PDMProxySession;
import zhongjyuan.integration.module.pdm.network.service.PDMProxyClient;
import zhongjyuan.integration.module.pdm.service.IAccountService;
import zhongjyuan.integration.module.pdm.service.IAgentService;
import zhongjyuan.integration.module.pdm.service.impl.AccountServiceImpl;
import zhongjyuan.integration.module.pdm.service.impl.AgentServiceImpl;

/**
 * @className: ServiceModule
 * @description: ServiceModule类是一个Guice依赖注入模块，用于配置PDM集成模块中的服务。
 * @author: zhongjyuan
 * @date: 2023年11月1日 下午5:34:32
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class ServiceModule extends AbstractModuleService {

	/**
	 * 配置Guice依赖注入容器。
	 */
	@Override
	protected void configurate() {

		// 将IoConnector接口绑定到NioSocketConnector类
		bind(IoConnector.class).to(NioSocketConnector.class);

		// 将AgentServiceImpl类和AccountServiceImpl类绑定到单例模式
		bind(AgentServiceImpl.class).in(Scopes.SINGLETON);
		bind(AccountServiceImpl.class).in(Scopes.SINGLETON);

		// 将PDMProxySession类绑定到单例模式
		bind(PDMProxySession.class).in(Scopes.SINGLETON);

		// 使用hosting方法将IAgentService接口和AgentServiceImpl类相关联，并将它们绑定到单例模式
		hosting(IAgentService.class).with(AgentServiceImpl.class).in(Scopes.SINGLETON);

		// 使用hosting方法将IAccountService接口和AccountServiceImpl类相关联，并将它们绑定到单例模式
		hosting(IAccountService.class).with(AccountServiceImpl.class).in(Scopes.SINGLETON);

		// 将PDMProxyClient类绑定到饱汉模式，并在启动时立即创建单例实例
		bind(PDMProxyClient.class).asEagerSingleton();
	}

}
