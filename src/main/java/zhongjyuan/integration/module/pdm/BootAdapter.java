package zhongjyuan.integration.module.pdm;

import java.util.Arrays;
import java.util.concurrent.Future;

import org.slf4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Module;

import zhongjyuan.integration.module.AbstractModuleService;
import zhongjyuan.integration.module.DefaultModuleListener;
import zhongjyuan.integration.module.IModuleInfo;
import zhongjyuan.integration.module.IModuleListener;
import zhongjyuan.integration.module.ModuleManage;
import zhongjyuan.integration.module.hotswap.AbstractHotSwapModuleAdapter;
import zhongjyuan.integration.module.network.MessageContext;
import zhongjyuan.integration.module.network.message.MessageManage;
import zhongjyuan.integration.module.network.message.MessageTokenException;
import zhongjyuan.integration.module.pdm.model.IUnitServerType;
import zhongjyuan.integration.module.pdm.model.UnitServerType;
import zhongjyuan.integration.module.pdm.network.service.PDMProxyClient;
import zhongjyuan.integration.module.pdm.service.IUnitServerService;
import zhongjyuan.integration.module.pdm.service.impl.UnitServerServiceImpl;
import zhongjyuan.integration.module.utils.Console;

/**
 * @className: BootAdapter
 * @description: 启动适配器，用于加载和启动应用程序。继承自{@link AbstractHotSwapModuleAdapter} ，并实现了相应的方法，用于热插拔模块的适配。
 * @author: zhongjyuan
 * @date: 2023年11月1日 下午5:37:35
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class BootAdapter extends AbstractHotSwapModuleAdapter {

	/**
	 * Logger 对象，用于记录日志。
	 */
	@Inject
	private Logger LOGGER;

	/**
	 * ModuleManage 对象，用于管理模块。
	 */
	@Inject
	private ModuleManage moduleManage;

	/**
	 * ServiceManage 对象，用于管理服务。
	 */
	@Inject
	private ServiceManage pdmService;

	/**
	 * PDMProxyClient 对象，用于连接到主数据代理。
	 */
	@Inject
	private PDMProxyClient client;

	/**
	 * IModuleListener 对象，用于监听模块事件。
	 */
	private IModuleListener listener;

	/**
	 * 构造函数，用于初始化 BootAdapter 对象。
	 *
	 * @param hotSwapModuleInfo 热插拔模块的信息。
	 */
	public BootAdapter(IModuleInfo hotSwapModuleInfo) {
		super(hotSwapModuleInfo);
	}

	/**
	 * 配置模块的方法，返回一个 Module 对象列表。
	 *
	 * @return Module 对象列表。
	 * @throws Exception 如果配置失败则抛出异常。
	 */
	@Override
	protected Iterable<? extends Module> configure() throws Exception {
		return Arrays.asList(new AbstractModuleService() {

			@Override
			protected void configurate() {
				hosting(IUnitServerService.class).with(UnitServerServiceImpl.class);

				hosting(IUnitServerType.class).with(UnitServerType.class);

				install(new ServiceModule());
			}

		});
	}

	/**
	 * 设置模块的扩展方法，用于在所有模块启动完成后启动向主数据代理的连接客户端。
	 *
	 * @throws Exception 如果设置扩展失败则抛出异常。
	 */
	@Override
	protected void setupExtension() throws Exception {
		super.setupExtension();

		// 当所有的模块都启动完成时，应该启动向主数据代理的连接客户端做主动连接。
		if (null != moduleManage) {

			listener = new DefaultModuleListener() {

				/**
				 * 当启动完成时，打印出 PDM 服务组件所挂载的数据包。
				 *
				 * @param moduleManage 模块管理器。
				 */
				@Override
				public void startFinished(ModuleManage moduleManage) {
					super.startFinished(moduleManage);

					// 打印出PDM服务组件所挂载的数据包。
					if (null != client) {
						try {
							Console.printSection("PDM I/O Message");

							final MessageContext context = client.getMessageContext();

							final String[] strings = MessageManage.treeMessages(context);
							for (String line : strings) {
								Console.print(line);
							}

							Console.printSection("PDM I/O Message");
						} catch (MessageTokenException e) {
							LOGGER.error("Exception caught at processing with the PDM I/O message context.");
						}
					}
				}

				/**
				 * 当启动时，发起连接请求并等待连接完成。
				 *
				 * @param moduleManage 模块管理器。
				 */
				@Override
				public void startNotify(final ModuleManage moduleManage) {
					if (null != pdmService) {
						// 发起连接请求
						Future<?> future = pdmService.tryingConnect();
						if (null != future) {
							try {
								future.get();
							} catch (final Exception e) {
							}
						}
					}
				}
			};

			moduleManage.addListener(listener);
		}
	}

	/**
	 * 关闭模块的方法，用于关闭 PDM 服务、连接客户端以及移除模块监听器。
	 *
	 * @throws Exception 如果关闭模块失败则抛出异常。
	 */
	@Override
	protected void shutdown() throws Exception {
		super.shutdown();

		pdmService.drop();

		client.stop();

		if (null != moduleManage && null != listener)
			moduleManage.removeListener(listener);
	}

	/**
	 * 启动模块的方法，在此处如果模块管理器已经初始化完成，则调用监听器的 startNotify 方法。
	 *
	 * @throws Exception 如果启动模块失败则抛出异常。
	 */
	@Override
	protected void startup() throws Exception {
		super.startup();

		if (null != moduleManage && moduleManage.hasInitialized()) {
			listener.startNotify(moduleManage);
		}
	}
}
