package zhongjyuan.integration.module.pdm.service.impl;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.inject.Inject;
import com.google.inject.name.Named;

import zhongjyuan.integration.module.pdm.model.IAgent;
import zhongjyuan.integration.module.pdm.model.IUnitServer;
import zhongjyuan.integration.module.pdm.model.IUnitServerType;
import zhongjyuan.integration.module.pdm.model.UnitServerType;
import zhongjyuan.integration.module.pdm.service.IUnitServerService;

public class UnitServerServiceImpl implements IUnitServerService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UnitServerServiceImpl.class);

	@Inject(
		optional = true)
	@Named("agent.ip.key")
	String IP = "ip";

	@Inject(
		optional = true)
	@Named("agent.port.key")
	String PORT = "port";

	@Inject(
		optional = true)
	@Named("agent.intranetip.key")
	String INTERNAL_IP = "intranetip";

	@Inject(
		optional = true)
	@Named("db.default.port")
	int DEFAULT_DB_PORT = 2433;

	@Inject(
		optional = true)
	@Named("agent.default.port")
	int DEFAULT_AGENT_PORT = 8888;

	@Inject(
		optional = true)
	@Named("business.default.port")
	int DEFAULT_BUSINESS_PORT = 8000;

	@Override
	public IUnitServer getRouteServer(IAgent agent) {
		if (agent != null) {

			IUnitServer unitServer = getUnitServer(agent, UnitServerType.WEB);
			if (unitServer == null) {
				unitServer = getUnitServer(agent, UnitServerType.IIS);
			}

			if (unitServer == null) {
				unitServer = getUnitServer(agent, UnitServerType.BUSINESS);
			}

			return unitServer;
		}

		return null;
	}

	@Override
	public IUnitServer getUnitServer(IAgent agent, IUnitServerType serverType) {

		Map<IUnitServerType, Map<Integer, IUnitServer>> unitServerTypeMap = agent.getUnitServers();
		if (unitServerTypeMap != null) {

			Map<Integer, IUnitServer> unitServerMap = unitServerTypeMap.get(serverType);
			if (unitServerMap != null) {
				if (serverType.getValue() == UnitServerType.DB2.getValue())
					return unitServerMap.get(0);

				return unitServerMap.get(1);
			}
		}

		return null;
	}

	@Override
	public IUnitServer getUnitServer(IAgent agent, IUnitServerType serverType, int main) {

		Map<IUnitServerType, Map<Integer, IUnitServer>> unitServerTypeMap = agent.getUnitServers();
		if (unitServerTypeMap != null) {

			Map<Integer, IUnitServer> unitServerMap = unitServerTypeMap.get(serverType);
			if (unitServerMap != null) {

				IUnitServer db1 = unitServerMap.get(main);
				LOGGER.error("======pdmtype:" + serverType.getName() + "||" + serverType.getValue());

				if (db1 != null && db1.getParameters() != null) {
					LOGGER.error("======pdm：db1 IP:" + db1.getParameters().get("intranetip"));
				}

				return unitServerMap.get(main);
			}
		}

		return null;
	}

	@Override
	public Map<Integer, IUnitServer> getUnitServers(IAgent agent, IUnitServerType serverType) {

		Map<IUnitServerType, Map<Integer, IUnitServer>> unitServerTypeMap = agent.getUnitServers();
		if (unitServerTypeMap != null) {
			return unitServerTypeMap.get(serverType);
		}

		return null;
	}

	@Override
	public int getPort(IUnitServer server) {
		int port = DEFAULT_AGENT_PORT;

		if (server.getType() == UnitServerType.DB.getValue()) {
			port = DEFAULT_DB_PORT;
		} else if (server.getType() == UnitServerType.DB2.getValue()) {
			port = DEFAULT_DB_PORT;
		} else if (server.getType() == UnitServerType.BUSINESS.getValue()) {
			port = DEFAULT_BUSINESS_PORT;
		}

		return port;
	}

	@Override
	public SocketAddress getAddress(IUnitServer server) {
		if (server == null) {
			throw new IllegalArgumentException("UnitServer");
		}

		Map<String, Object> map = server.getParameters();

		if (map != null) {
			// 设置默认端口
			int port = getPort(server);
			int defaultPort = port;

			String portStr = null, hostStr = null;
			if (map.containsKey(IP)) {
				hostStr = map.get(IP).toString();
			}

			if (map.containsKey(PORT)) {
				portStr = map.get(PORT).toString();
			}

			if (!Strings.isNullOrEmpty(portStr)) {
				try {
					port = Integer.valueOf(portStr);
				} catch (NumberFormatException ne) {
					port = defaultPort;
				}
			}

			try {
				if (Strings.isNullOrEmpty(hostStr) || hostStr.equals("*")) {
					return new InetSocketAddress(port);
				} else {
					return new InetSocketAddress(hostStr, port);
				}
			} catch (IllegalArgumentException iae) {
				port = (short) port;
				port = port > 0 ? port : getPort(server);
				return new InetSocketAddress(hostStr, port);
			}
		}

		return null;
	}

	@Override
	public String getTypeName(int value) {
		// TODO Auto-generated method stub
		return null;
	}

}
