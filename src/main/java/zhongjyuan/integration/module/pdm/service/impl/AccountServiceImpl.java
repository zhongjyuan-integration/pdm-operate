package zhongjyuan.integration.module.pdm.service.impl;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;

import com.google.common.base.Strings;
import com.google.inject.Inject;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.integration.module.pdm.message.AuthDuplexSyncMessage;
import zhongjyuan.integration.module.pdm.message.AuthDuplexSyncEncryptMessage;
import zhongjyuan.integration.module.pdm.message.PasswordDuplexSyncModifyMessage;
import zhongjyuan.integration.module.pdm.model.IAccount;
import zhongjyuan.integration.module.pdm.model.IAuthContext;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.network.message.SyncMessageRecord;
import zhongjyuan.integration.module.pdm.message.AuthDuplexSyncMD5Message;
import zhongjyuan.integration.module.pdm.network.PDMProxySession;
import zhongjyuan.integration.module.pdm.service.IAccountService;

public class AccountServiceImpl implements IAccountService {

	@Inject
	private Logger LOGGER;

	private ObjectMapper objectMapper;

	private final PDMProxySession session;

	@Inject
	public AccountServiceImpl(PDMProxySession session) {
		this.objectMapper = new ObjectMapper();
		this.session = session;
	}

	@Override
	public ResponseResult<IAccount> authenticated(IAuthContext authContext) throws Exception {
		try {
			AuthDuplexSyncMessage message = new AuthDuplexSyncMessage(authContext);
			SyncMessageRecord record = session.write(message);

			if (record != null && record.getResult() instanceof AuthDuplexSyncMessage) {
				message = (AuthDuplexSyncMessage) record.getResult();

				Object rawObject = message.getRawObject();
				ResponseCode responseCode = message.getResponseCode();

				if (rawObject instanceof IAccount || rawObject == null) {
					IAccount account = (IAccount) rawObject;
					return new ResponseResult<IAccount>().withResponseCode(responseCode).withData(account);
				} else if (rawObject instanceof String) {
					return objectMapper.readValue(rawObject.toString(), new TypeReference<ResponseResult<IAccount>>() {

					});
				}
			}
		} catch (final Exception e) {
			LOGGER.error("连接PDM主数据获取用户登录验证信息出错：{}", e.getMessage(), e);
		}

		return null;
	}

	@Override
	public ResponseResult<IAccount> authenticatedByMD5(IAuthContext authContext) throws Exception {
		try {
			AuthDuplexSyncMD5Message message = new AuthDuplexSyncMD5Message(authContext);
			SyncMessageRecord record = session.write(message);

			if (record != null && record.getResult() instanceof AuthDuplexSyncMD5Message) {
				message = (AuthDuplexSyncMD5Message) record.getResult();

				Object rawObject = message.getRawObject();
				ResponseCode responseCode = message.getResponseCode();

				if (rawObject instanceof IAccount || rawObject == null) {
					IAccount account = (IAccount) rawObject;
					return new ResponseResult<IAccount>().withResponseCode(responseCode).withData(account);
				} else if (rawObject instanceof String) {
					return objectMapper.readValue(rawObject.toString(), new TypeReference<ResponseResult<IAccount>>() {

					});
				}
			}
		} catch (final Exception e) {
			LOGGER.error("连接PDM主数据获取用户登录验证信息出错：{}", e.getMessage(), e);
		}

		return null;
	}

	@Override
	public ResponseResult<IAccount> authentryenticated(IAuthContext authContext) throws Exception {
		try {
			AuthDuplexSyncEncryptMessage message = new AuthDuplexSyncEncryptMessage(authContext);
			SyncMessageRecord record = session.write(message);

			if (record != null && record.getResult() instanceof AuthDuplexSyncEncryptMessage) {
				message = (AuthDuplexSyncEncryptMessage) record.getResult();

				Object rawObject = message.getRawObject();
				ResponseCode responseCode = message.getResponseCode();

				if (rawObject instanceof IAccount || rawObject == null) {
					IAccount account = (IAccount) rawObject;
					return new ResponseResult<IAccount>().withResponseCode(responseCode).withData(account);
				} else if (rawObject instanceof String || rawObject instanceof ResponseResult<?>) {
					return objectMapper.readValue(rawObject.toString(), new TypeReference<ResponseResult<IAccount>>() {

					});
				}
			}
		} catch (final Exception e) {
			LOGGER.error("连接PDM主数据获取用户登录验证信息出错：{}", e.getMessage(), e);
		}

		return null;
	}

	@Override
	public ResponseResult<Boolean> modifyPassword(String account, String oldPass, String newPass, String ip) throws Exception {
		if (Strings.isNullOrEmpty(account))
			throw new IllegalArgumentException("account");

		if (Strings.isNullOrEmpty(oldPass) || Strings.isNullOrEmpty(newPass) || Strings.isNullOrEmpty(ip))
			throw new IllegalArgumentException("password datas!");

		try {
			PasswordDuplexSyncModifyMessage message = new PasswordDuplexSyncModifyMessage(account, oldPass, newPass, ip);
			SyncMessageRecord record = session.write(message);

			if (null != record && (record.getResult() instanceof String || record.getResult() instanceof ResponseResult<?>)) {
				return objectMapper.readValue(record.getResult().toString(), new TypeReference<ResponseResult<Boolean>>() {

				});
			}
		} catch (final Exception e) {
			LOGGER.error("连接PDM主数据密码修改信息出错：{}", e.getMessage(), e);
		}

		return null;
	}

}
