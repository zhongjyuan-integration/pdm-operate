package zhongjyuan.integration.module.pdm.service.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;

import com.google.common.base.Strings;
import com.google.common.collect.MapMaker;
import com.google.inject.Inject;

import zhongjyuan.event.AbstractEventSubscribe;
import zhongjyuan.event.Event;
import zhongjyuan.integration.module.GlobalConfigurate;
import zhongjyuan.integration.module.network.message.SyncMessageRecord;
import zhongjyuan.integration.module.pdm.message.AgentDuplexSyncSelectMessage;
import zhongjyuan.integration.module.pdm.model.IAgent;
import zhongjyuan.integration.module.pdm.model.IUnitServer;
import zhongjyuan.integration.module.pdm.model.IUnitServerType;
import zhongjyuan.integration.module.pdm.network.PDMProxySession;
import zhongjyuan.integration.module.pdm.service.IAgentService;

public class AgentServiceImpl extends AbstractEventSubscribe<String, Event<String>> implements IAgentService {

	@Inject
	private Logger LOGGER;

	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock(false);

	private final Lock w = lock.writeLock();

	private final PDMProxySession session;

	protected PDMProxySession getSession() {
		return session;
	}

	private Map<Integer, IAgent> agents;

	private Map<String, IAgent> agentSites;

	private Map<Integer, IAgent> agentTree;

	@Inject
	public AgentServiceImpl(PDMProxySession session) {
		if (null == session)
			throw new IllegalArgumentException("illegal session!");

		this.session = session;
	}

	@Override
	public void clear() {
		w.lock();
		try {
			if (null != agents)
				agents.clear();

			if (null != agentSites)
				agentSites.clear();

			if (null != agentTree)
				agentTree.clear();
		} finally {
			w.unlock();
		}
	}

	@Override
	public Collection<IAgent> getRootAgents() {
		try {
			if (agentTree != null && !agentTree.isEmpty())
				return agentTree.values();

			getAgentHashSet();

			if (agents != null)
				makeTree();

			return agentTree.values();
		} finally {
		}
	}

	@Override
	public Collection<IAgent> getAgentHashSet() {
		long time = System.currentTimeMillis();

		w.lock();
		try {

			if (agents != null && !agents.isEmpty()) {
				return agents.values();
			}

			try {
				AgentDuplexSyncSelectMessage message = new AgentDuplexSyncSelectMessage((byte) 1);
				SyncMessageRecord struct = getSession().write(message);

				agents = resolveMessage(struct);
				if (agents != null) {
					makeSite(agents);
					makeFilter(agents);
					return agents.values();
				}
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}

			return null;
		} finally {
			w.unlock();
			LOGGER.info("Handle the data time in {} ms.", System.currentTimeMillis() - time);
		}
	}

	@Override
	public IAgent getAgentBySite(String site) {
		try {
			if (agentSites != null && !agentSites.isEmpty()) {
				return agentSites.get(site);
			}
		} finally {

		}

		return null;
	}

	@Override
	public IAgent getAgentById(int agentId) {
		try {
			if (agents == null || agents.isEmpty()) {
				getAgentHashSet();
			}

			return agents.get(agentId);
		} finally {

		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends IAgent> T getAgentById(int agentId, Class<T> drivedClass) {
		return (T) getAgentById(agentId);
	}

	@SuppressWarnings("unchecked")
	private Map<Integer, IAgent> resolveMessage(SyncMessageRecord messageRecord) {
		if (messageRecord != null && messageRecord.getResult() != null && messageRecord.getResult() instanceof Map) {

			ConcurrentMap<Integer, IAgent> map = new MapMaker().concurrencyLevel(GlobalConfigurate.AVAILABLE_PROCESSORS).makeMap();

			map.putAll((Map<Integer, IAgent>) messageRecord.getResult());

			return map;
		}

		return null;
	}

	private void makeSite(Map<Integer, IAgent> agents) {
		if (agentSites == null || agentSites.isEmpty()) {

			w.lock();
			try {
				if (agents != null) {
					agentSites = new MapMaker().concurrencyLevel(GlobalConfigurate.AVAILABLE_PROCESSORS).makeMap();

					Iterator<Entry<Integer, IAgent>> iterator = agents.entrySet().iterator();
					while (iterator.hasNext()) {

						Entry<Integer, IAgent> agentEntry = iterator.next();

						IAgent agent = agentEntry.getValue();
						if (agent.getStatus() != -1) {

							Map<String, Object> paramters = agent.getParamters();
							if (paramters != null && paramters.size() > 0) {

								Object obj = paramters.get("site");
								if (obj != null) {
									String site = obj.toString();
									if (!Strings.isNullOrEmpty(site)) {
										agentSites.put(site, agent);
									}
								}
							}
						}
					}
				}
			} finally {
				w.unlock();
			}

		}
	}

	protected void makeFilter(Map<Integer, IAgent> agents) {
		// 过滤所有不符合状态的游戏区以及代服务器
		if (agents != null) {

			Iterator<Entry<Integer, IAgent>> iterator = agents.entrySet().iterator();
			while (iterator.hasNext()) {

				Entry<Integer, IAgent> agentEntry = iterator.next();

				IAgent agent = agentEntry.getValue();
				if (agent.getStatus() <= 0) {

					IAgent parent = agent.getParent();
					if (parent != null && parent.getChildren() != null) {
						parent.getChildren().remove(agent);
					}

					iterator.remove();
					continue;
				}

				Iterator<Entry<IUnitServerType, Map<Integer, IUnitServer>>> it = agent.getUnitServers().entrySet().iterator();
				while (it.hasNext()) {

					Entry<IUnitServerType, Map<Integer, IUnitServer>> entry = it.next();

					Map<Integer, IUnitServer> unitServerMap = entry.getValue();

					Iterator<Entry<Integer, IUnitServer>> iit = unitServerMap.entrySet().iterator();
					while (iit.hasNext()) {
						Entry<Integer, IUnitServer> i_entry = iit.next();
						if (i_entry.getValue().getStatus() != 1) {
							iit.remove();
						}
					}
				}
			}
		}
	}

	private void makeTree() {
		agentTree = new MapMaker().concurrencyLevel(GlobalConfigurate.AVAILABLE_PROCESSORS).makeMap();
		
		for (IAgent agent : agents.values()) {
			IAgent parent = agent.getParent();
			if (parent == null || parent.getId() == 0) {
				agentTree.put(agent.getId(), agent);
			}
		}
		
		LOGGER.info("构造代理树形结构对象完成！");
	}
}
