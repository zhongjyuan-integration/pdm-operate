package zhongjyuan.integration.module.pdm;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.mina.core.session.IoSession;
import org.slf4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import zhongjyuan.integration.module.pdm.network.PDMProxySession;

/**
 * @className: ServiceManage
 * @description: 用于管理PDM代理服务的类
 * @author: zhongjyuan
 * @date: 2023年11月1日 下午5:17:55
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Singleton
public class ServiceManage {

	/**
	 * 日志记录器
	 */
	@Inject
	private Logger LOGGER;

	/**
	 * PDM代理会话
	 */
	private final PDMProxySession session;

	/**
	 * 构造函数
	 * 
	 * @param pdmProxySession PDM代理会话
	 */
	@Inject
	private ServiceManage(PDMProxySession session) {
		if (null == session)
			throw new IllegalArgumentException("illegal session for PDMClient proxy!");

		this.session = session;
	}

	/**
	 * 尝试连接PDM代理服务端
	 * 
	 * @return 连接操作的Future对象
	 */
	public Future<?> tryingConnect() {
		ExecutorService executor = Executors.newSingleThreadExecutor();
		try {
			return executor.submit(() -> {
				LOGGER.info("正在尝试连接PDM代理...");

				IoSession ioSession = session.getSession();
				if (null != ioSession) {
					LOGGER.info("已连接PDM代理服务端！");
				} else {
					LOGGER.info("连接不上PDM代理服务端！");
				}

				return ioSession;
			});
		} finally {
			executor.shutdown();
		}
	}

	/**
	 * 关闭PDM代理服务，释放资源
	 */
	public void drop() {
		if (null != this.session)
			this.session.close();
	}
}
