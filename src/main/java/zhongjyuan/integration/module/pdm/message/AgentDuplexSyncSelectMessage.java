package zhongjyuan.integration.module.pdm.message;

import org.apache.mina.core.session.IoSession;

import com.google.inject.Inject;

import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.io.IPacketBufferReader;
import zhongjyuan.integration.module.network.io.IPacketBufferWriter;
import zhongjyuan.integration.module.network.message.AbstractDuplexSyncMessage;
import zhongjyuan.integration.module.network.message.SyncMessageRecord;

@Token(
	id = 0x1F9,
	stateMasks = { 0 },
	explosed = false,
	desc = "代理双工同步消息查询数据封包")
public class AgentDuplexSyncSelectMessage extends AbstractDuplexSyncMessage {

	/**
	 * 0: NONE, 1: ALL, 2: PARTI
	 */
	private byte type = 0;

	private Object rawObject;

	@Inject
	public AgentDuplexSyncSelectMessage() {
		super();
	}

	public AgentDuplexSyncSelectMessage(byte type) {
		this.type = type;
	}

	@Override
	protected void synchronizedRun(IoSession session) throws Exception {
		SyncMessageRecord struct = getMessageRecord(session);
		if (struct != null) {
			struct.setResult(rawObject);
		}
	}

	@Override
	protected void writeContent(IPacketBufferWriter writer) throws Exception {
		writer.writeByte(type);
		writer.writeBoolean(true);
	}

	@Override
	protected void readContent(IPacketBufferReader reader) throws Exception {
		if (reader.readBoolean()) {
			// 表示有数据传输过来。
			rawObject = reader.readObject();
		}
	}

}
