package zhongjyuan.integration.module.pdm.message;

import com.google.inject.Inject;

import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.annotation.UnDispatchable;
import zhongjyuan.integration.module.network.io.IPacketBufferReader;
import zhongjyuan.integration.module.network.io.IPacketBufferWriter;
import zhongjyuan.integration.module.network.message.AbstractDuplexMessage;
import zhongjyuan.integration.module.network.message.MessageReceipt;
import zhongjyuan.integration.module.pdm.model.IAgent;
import zhongjyuan.integration.module.pdm.service.IAgentService;

@Token(
	id = 0x1FA,
	stateMasks = { 0 },
	explosed = false,
	desc = "代理双工同步消息查询名称数据封包")
@UnDispatchable
public class AgentDuplexSelectNameMessage extends AbstractDuplexMessage {

	@Inject
	IAgentService agentService;

	private String site;

	private String name = "";

	@Override
	protected void writeContent(IPacketBufferWriter writer) throws Exception {
		writer.writePrefixString(name);
	}

	@Override
	protected void readContent(IPacketBufferReader reader) throws Exception {
		site = reader.readPrefixString();
	}

	@Override
	protected void execute(MessageReceipt receipt) throws Exception {
		IAgent agent = agentService.getAgentBySite(site);
		if (agent != null) {
			name = agent.getName();
		}
		receipt.write(this);
	}

}
