package zhongjyuan.integration.module.pdm.message;

import javax.annotation.Nullable;

import org.apache.mina.core.session.IoSession;

import com.google.inject.Inject;

import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.io.IPacketBufferReader;
import zhongjyuan.integration.module.network.io.IPacketBufferWriter;
import zhongjyuan.integration.module.network.message.AbstractDuplexSyncMessage;
import zhongjyuan.integration.module.pdm.model.IAccount;
import zhongjyuan.integration.module.pdm.model.IAuthContext;

@Token(
	id = 0x1F5,
	stateMasks = { 0 },
	explosed = false,
	desc = "认证双工同步消息数据封包")
public class AuthDuplexSyncMessage extends AbstractDuplexSyncMessage {

	private Object rawObject;

	private ResponseCode responseCode;

	private final IAuthContext authContext;

	public AuthDuplexSyncMessage() {
		this.authContext = null;
	}

	@Inject
	public AuthDuplexSyncMessage(@Nullable IAuthContext authContext) {
		this.authContext = authContext;
	}

	@Override
	protected void synchronizedRun(IoSession session) throws Exception {
		getMessageRecord(session).setResult(this);
	}

	@Override
	protected void writeContent(IPacketBufferWriter writer) throws Exception {

		writer.writeBoolean(!(this.authContext == null));

		if (this.authContext != null) {
			writer.writePrefixString(String.valueOf(authContext.getAccountId()));
			writer.writePrefixString(this.authContext.getPassword());
			writer.writePrefixString(this.authContext.getRemoteIp());
			writer.writePrefixString("1.0.0.0");
		}
	}

	@Override
	protected void readContent(IPacketBufferReader reader) throws Exception {
		this.responseCode = (ResponseCode) reader.readEnum(ResponseCode.class);
		switch (this.responseCode) {
			case ERROR:
			case SUCCEED:
			case FAILURE:
			case UNKONWN:
			case PASSWORD_WRONG:
				if (reader.readBoolean())
					this.rawObject = reader.readObject();
				break;
			default:
				break;
		}
	}

	public Object getRawObject() {
		return this.rawObject;
	}

	public ResponseCode getResponseCode() {
		return this.responseCode;
	}

	public IAccount getAccount() {
		IAccount account = null;

		if ((this.rawObject != null) && ((this.rawObject instanceof IAccount))) {
			account = (IAccount) this.rawObject;
		}

		return account;
	}

}
