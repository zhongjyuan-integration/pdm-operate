package zhongjyuan.integration.module.pdm.message;

import javax.annotation.Nullable;

import org.apache.mina.core.session.IoSession;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;

import com.google.inject.Inject;

import zhongjyuan.domain.response.ResponseResult;
import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.io.IPacketBufferReader;
import zhongjyuan.integration.module.network.io.IPacketBufferWriter;
import zhongjyuan.integration.module.network.message.AbstractDuplexSyncMessage;
import zhongjyuan.integration.module.pdm.model.IAccount;
import zhongjyuan.integration.module.pdm.model.IAuthContext;

@Token(
	id = 0x1F6,
	stateMasks = { 0 },
	explosed = false,
	desc = "认证双工同步消息MD5数据封包")
public class AuthDuplexSyncMD5Message extends AbstractDuplexSyncMessage {

	@Inject
	Logger log;

	private Object rawObject;

	private String authResponse;

	private ResponseCode responseCode;

	private final IAuthContext authContext;

	public AuthDuplexSyncMD5Message() {
		super();
		this.authContext = null;
	}

	@Inject
	public AuthDuplexSyncMD5Message(@Nullable IAuthContext authContext) {
		super();
		this.authContext = authContext;
	}

	@Override
	protected void synchronizedRun(IoSession session) throws Exception {
		getMessageRecord(session).setResult(this);
	}

	@Override
	protected void writeContent(IPacketBufferWriter writer) throws Exception {
		writer.writeBoolean(authContext == null ? false : true);
		if (authContext != null) {
			writer.writePrefixString(String.valueOf(authContext.getAccountId()));
			writer.writePrefixString(authContext.getPassword());
			writer.writePrefixString(authContext.getRemoteIp());
			writer.writePrefixString("1.0.0.0");
		}
	}

	@Override
	protected void readContent(IPacketBufferReader reader) throws Exception {
		this.responseCode = (ResponseCode) reader.readEnum(ResponseCode.class);
		switch (this.responseCode) {
			case ERROR:
			case SUCCEED:
			case FAILURE:
			case UNKONWN:
			case PASSWORD_WRONG:
				if (reader.readBoolean())
					this.authResponse = reader.readPrefixString();
				break;
			default:
				break;
		}
	}

	public Object getRawObject() {
		return this.rawObject;
	}

	public ResponseCode getResponseCode() {
		return this.responseCode;
	}

	public IAccount getAccount() {
		IAccount account = null;

		if ((this.rawObject != null) && ((this.rawObject instanceof IAccount))) {
			account = (IAccount) this.rawObject;
		}

		return account;
	}

	@SuppressWarnings("unchecked")
	public ResponseResult<IAccount> readAccount(ObjectMapper objectMapper) {
		try {
			rawObject = objectMapper.readValue(this.authResponse, new TypeReference<ResponseResult<IAccount>>() {
			});
		} catch (Exception e) {
			log.error("read auth response error", e);
		}
		return (ResponseResult<IAccount>) rawObject;
	}
}
