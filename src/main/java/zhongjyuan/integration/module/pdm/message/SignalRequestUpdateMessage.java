package zhongjyuan.integration.module.pdm.message;

import org.slf4j.Logger;

import com.google.inject.Inject;

import zhongjyuan.event.Event;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.io.IPacketBufferReader;
import zhongjyuan.integration.module.network.message.AbstractRequestMessage;
import zhongjyuan.integration.module.network.message.MessageReceipt;
import zhongjyuan.integration.module.pdm.service.IAccountService;
import zhongjyuan.integration.module.pdm.service.IAgentService;

/**
 * @className: SignalRequestUpdateMessage
 * @description: 信号更新消息数据封包
 * @author: zhongjyuan
 * @date: 2023年11月1日 下午5:14:25
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
@Token(
	id = 0x1FB,
	stateMasks = { 0 },
	explosed = false,
	desc = "信号请求消息更新数据封包")
public class SignalRequestUpdateMessage extends AbstractRequestMessage {

	@Inject
	private Logger LOGGER;

	@Inject
	IAgentService agentService;

	@Inject
	IAccountService accountService;

	/**
	 * 需要更新的信号ID数组
	 */
	private int[] ids;

	/**
	 * 信号类型
	 */
	private int catalog = 0;

	@Override
	protected void readContent(IPacketBufferReader reader) throws Exception {
		this.catalog = reader.readByte();
		if (reader.hasRemaining() && reader.readBoolean()) {
			int count = reader.readShort();
			ids = new int[count];
			for (int i = 0; i < count; i++) {
				ids[i] = reader.readInt();
			}
		}
	}

	@Override
	public void execute(MessageReceipt receipt) throws Exception {
		// 更新代理数据并通知客户端更新代理树形结构。

		LOGGER.info("Run the update signal message! the catalog is {},ids-{}", catalog, ids);

		switch (this.catalog) {
			case 1:
				// 代理数据
				agentService.clear();
				agentService.getRootAgents();
				LOGGER.error("+++++++++++++++++" + ids.length);

				// 同步执行
				if (ids != null && ids.length > 0) {
					// 事件更新
					Event<String> event = new Event<>(IAgentService.EVENT_UPDATED);
					event.setValue(ids);
					agentService.notify(event);
				}
				break;
			case 2:
				// 用户数据
				break;
			case 3:
			default:
				// nothing to do...
				break;
		}
	}
}
