package zhongjyuan.integration.module.pdm.message;

/**
 * 501 - 600
 * 
 * AuthDuplexSyncMessage => 0x1F5(501)
 * AuthDuplexSyncMD5Message => 0x1F6(502)
 * AuthDuplexSyncEncryptMessage => 0x1F7(503)
 * PasswordDuplexSyncModifyMessage => 0x1F8(504)
 * AgentDuplexSyncSelectMessage => 0x1F9(505)
 * AgentDuplexSelectNameMessage => 0x1FA(506)
 * SignalRequestUpdateMessage => 0x1FB(507)
 */