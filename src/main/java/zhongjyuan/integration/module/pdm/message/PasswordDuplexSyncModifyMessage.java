package zhongjyuan.integration.module.pdm.message;

import org.apache.mina.core.session.IoSession;

import com.google.common.base.Strings;
import com.google.inject.Inject;

import zhongjyuan.integration.domain.ResponseCode;
import zhongjyuan.integration.module.network.annotation.Token;
import zhongjyuan.integration.module.network.io.IPacketBufferReader;
import zhongjyuan.integration.module.network.io.IPacketBufferWriter;
import zhongjyuan.integration.module.network.message.AbstractDuplexSyncMessage;

@Token(
	id = 0x1F8,
	stateMasks = { 0 },
	explosed = false,
	desc = "密码双工同步消息修改数据封包")
public class PasswordDuplexSyncModifyMessage extends AbstractDuplexSyncMessage {

	private String ip;

	private String userName;

	private String oldPass;

	private String newPass;

	private String response;

	@Inject
	public PasswordDuplexSyncModifyMessage() {
		super();
	}

	public PasswordDuplexSyncModifyMessage(String userName, String oldPass, String newPass, String ip) {
		super();
		this.userName = userName;
		this.oldPass = oldPass;
		this.newPass = newPass;
		this.ip = ip;
	}

	@Override
	protected void synchronizedRun(IoSession session) throws Exception {
		this.getMessageRecord(session).setResult(response);
	}

	@Override
	protected void writeContent(IPacketBufferWriter writer) throws Exception {
		if (!Strings.isNullOrEmpty(userName) && !Strings.isNullOrEmpty(oldPass) && !Strings.isNullOrEmpty(newPass) && !Strings.isNullOrEmpty(ip)) {
			writer.writeBoolean(true);
			writer.writePrefixString(userName);
			writer.writePrefixString(oldPass);
			writer.writePrefixString(newPass);
			writer.writePrefixString(ip);
		} else {
			writer.writeBoolean(false);
		}
	}

	@Override
	protected void readContent(IPacketBufferReader reader) throws Exception {
		reader.readEnum(ResponseCode.class);
		if (reader.hasRemaining() && reader.readBoolean()) {
			response = reader.readPrefixString();
		}
	}

}
