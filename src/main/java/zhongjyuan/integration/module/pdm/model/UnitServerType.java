package zhongjyuan.integration.module.pdm.model;

import java.util.List;

import com.google.common.collect.Lists;

/**
 * @className: UnitServerType
 * @description: 单元服务类型对象.实现{@link IUnitServerType}
 * 
 * <p>该类具有以下字段：</p>
 * <ul>
 * 	<li>id：表示单元服务类型的ID。</li>
 * 	<li>name：表示单元服务类型的名称。</li>
 * 	<li>value：表示单元服务类型的值。</li>
 * </ul>
 * 
 * @author: zhongjyuan
 * @date: 2023年11月1日 下午4:06:44
 * @version: 2023.02.01
 * @copyright: Copyright (c) 2023 zhongjyuan.com
 */
public class UnitServerType implements IUnitServerType {

	private static final long serialVersionUID = 1L;

	/**
	 * 所有预定义单元服务类型的列表
	 */
	private static final List<IUnitServerType> VALUES = Lists.newArrayList();

	/**
	 * 表示Web服务的预定义单元服务类型对象
	 */
	public static final UnitServerType WEB = new UnitServerType(1, "web", 1);

	/**
	 * 表示IIS服务的预定义单元服务类型对象
	 */
	public static final UnitServerType IIS = new UnitServerType(2, "iis", 2);

	/**
	 * 表示主数据库服务的预定义单元服务类型对象
	 */
	public static final UnitServerType DB = new UnitServerType(3, "masterDB", 3);

	/**
	 * 表示从数据库服务的预定义单元服务类型对象
	 */
	public static final UnitServerType DB2 = new UnitServerType(4, "slaveDB", 4);

	/**
	 * 表示网关服务的预定义单元服务类型对象
	 */
	public static final UnitServerType GATEWAY = new UnitServerType(5, "gateway", 5);

	/**
	 * 表示业务服务的预定义单元服务类型对象
	 */
	public static final UnitServerType BUSINESS = new UnitServerType(6, "business", 6);

	/**
	 * 表示聊天服务的预定义单元服务类型对象
	 */
	public static final UnitServerType CHAT = new UnitServerType(7, "chat", 7);

	/**
	 * 表示战斗服务的预定义单元服务类型对象
	 */
	public static final UnitServerType FIGHT = new UnitServerType(8, "fight", 8);

	/**
	 * 表示王城服务的预定义单元服务类型对象
	 */
	public static final UnitServerType CASTLE = new UnitServerType(9, "castle", 9);

	/**
	 * 表示中心服务的预定义单元服务类型对象
	 */
	public static final UnitServerType CENTER = new UnitServerType(10, "center", 10);

	/**
	 * 单元服务类型的ID
	 */
	private final int id;

	/**
	 * 单元服务类型的名称
	 */
	private final String name;

	/**
	 * 单元服务类型的值
	 */
	private final int value;

	/**
	 * 私有构造函数，创建单元服务类型对象
	 * 
	 * @param id    单元服务类型的ID
	 * @param name  单元服务类型的名称
	 * @param value 单元服务类型的值
	 */
	private UnitServerType(int id, String name, int value) {
		this.id = id;
		this.name = name;
		this.value = value;

		VALUES.add(this);
	}

	/**
	 * 返回单元服务类型的ID
	 * 
	 * @return 单元服务类型的ID
	 */
	@Override
	public int getId() {
		return id;
	}

	/**
	 * 返回单元服务类型的名称
	 * 
	 * @return 单元服务类型的名称
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * 返回单元服务类型的值
	 * 
	 * @return 单元服务类型的值
	 */
	@Override
	public int getValue() {
		return value;
	}

	/**
	 * 比较两个单元服务类型对象是否相等
	 * 
	 * @param obj 要比较的对象
	 * @return 如果两个对象相等则返回true，否则返回false
	 */
	@Override
	public boolean equals(Object obj) {
		boolean result = true;
		result = (result) && (obj instanceof IUnitServerType);
		result = (result) && (((IUnitServerType) obj).getValue() == getValue());
		return result;
	}

	/**
	 * 返回单元服务类型的哈希码
	 * 
	 * @return 单元服务类型的哈希码
	 */
	@Override
	public int hashCode() {
		return getValue();
	}

	/**
	 * 根据给定的值返回对应的单元服务类型实例
	 * 
	 * @param value 给定的值
	 * @return 单元服务类型实例
	 * @throws IllegalArgumentException 如果给定的值无效，则抛出该异常
	 */
	public static IUnitServerType valueOf(int value) {
		for (IUnitServerType unitServerType : VALUES) {
			if (unitServerType.getValue() == value) {
				return unitServerType;
			}
		}

		throw new IllegalArgumentException("Invalid value for IUnitServerType: " + value);
	}
}
